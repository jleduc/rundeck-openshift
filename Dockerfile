FROM cern/cc7-base

EXPOSE 4440

# Configure env variables
# To check available rundeck versions https://bintray.com/rundeck/rundeck-rpm/rundeck
ARG RUNDECK_VERSION='2.11.14'
ENV RDECK_BASE '/var/lib/rundeck'
ENV RDECK_CONFIG '/etc/rundeck'

# Where to store the DB and project definitions and logs
VOLUME ["/var/rundeck", "/var/lib/rundeck/logs"]

# Install java requirement
# xmlstarlet is used to manipulate XML in run.sh
# CERN plugins dependencies: python-requests python-requests-kerberos bc openssh-clients cern-wrappers
# Operations: jq & screen are useful for cleanup of old jobs cf. https://gitlab.cern.ch/paas-tools/rundeck-openshift/-/merge_requests/24
RUN yum install -y java-1.8.0-openjdk gettext xmlstarlet \
    python-requests python-requests-kerberos bc openssh-clients cern-wrappers \
    jq screen && yum clean all 

# As per https://github.com/rundeck/docs/blob/3.4.x/docs/administration/install/linux-rpm.md#manual-yum-setup-1
COPY rundeck.repo /etc/yum.repos.d/

# Install rundeck
# See https://github.com/rundeck/rundeck/issues/5168#issuecomment-522818928 regarding the `--setopt=obsoletes=0` flag
RUN yum install -y --setopt=obsoletes=0 rundeck-${RUNDECK_VERSION} rundeck-config-${RUNDECK_VERSION} && \
    yum clean all

# Install CERN plugins
RUN cd /var/lib/rundeck/libext/ && \
    curl -L -O https://github.com/cernops/rundeck-exec-kerberos/releases/download/v1.4.1/rundeck-ssh-krb-node-executor-plugin-1.4.1.zip && \
    curl -L -O https://github.com/cernops/rundeck-puppetdb-nodes/releases/download/v1.4.1/rundeck-puppetdb-nodes-plugin-1.4.1.zip

COPY run.sh /

# Create rundeck folders and give appropriate permissions
RUN mkdir -p $RDECK_BASE && chmod -R a+rw $RDECK_BASE && chmod -R a+rw /var/log/rundeck && \
    chmod -R a+rw /tmp/rundeck && mkdir -p /rundeck-config && chmod -R a+rw $RDECK_CONFIG && \
    chmod -R a+rwx /rundeck-config && chmod a+x /run.sh

ENTRYPOINT './run.sh'
